# es-utils

Helper functions for Elasticsearch.

[![Clojars Project](https://img.shields.io/clojars/v/lt.tokenmill/es-utils.svg)](https://clojars.org/lt.tokenmill/es-utils)

## configuration format
```
{:docs
 {:host  "http://127.0.0.1:9200"
  :alias "docs"
  :conf  "docs.json"
  :data  "test-docs.json"}}
```
