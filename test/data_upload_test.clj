(ns data-upload-test
  (:require [clojure.test :refer :all]
            [org.httpkit.client :as http]
            [cheshire.core :as cheshire]
            [es-utils :as es-utils]))

(deftest ^:integration uploading
  (let [es-host (or (System/getenv "ES_HOST") "http://127.0.0.1:9200")
        es-index-confs {:docs {:host  es-host
                               :alias "docs"
                               :conf  "docs.json"
                               :data  "test-docs.json"}}]
    (when-not (es-utils/es-ready? es-host "ES" 30)
      (throw (RuntimeException. (format "ES %s is not ready" es-host))))
    (es-utils/prepare-indices {} nil es-index-confs)
    (es-utils/load-test-data {} nil es-index-confs)
    (is (es-utils/exists? (-> es-index-confs :docs :host)
                          (-> es-index-confs :docs :alias)))
    (is (= 1 (-> @(http/request {:url    (format "%s/%s/_count" es-host (-> es-index-confs :docs :alias))
                                 :method :get})
                 :body
                 (cheshire/decode true)
                 :count)))))

(deftest ^:integration error-logging
  (let [es-host (or (System/getenv "ES_HOST") "http://127.0.0.1:9200")
        es-index-confs {:docs {:host  es-host
                               :alias "docs"
                               :conf  "strict-schema.json"
                               :data  "test-docs.json"}}]
    (when-not (es-utils/es-ready? es-host "ES" 30)
      (throw (RuntimeException. (format "ES %s is not ready" es-host))))
    (es-utils/delete-all-docs es-host es-index-confs)
    (es-utils/prepare-indices {} nil es-index-confs)
    (es-utils/load-test-data {} nil es-index-confs)
    (is (es-utils/exists? (-> es-index-confs :docs :host)
                          (-> es-index-confs :docs :alias)))
    (is (= 0 (-> @(http/request {:url    (format "%s/%s/_count" es-host (-> es-index-confs :docs :alias))
                                 :method :get})
                 :body
                 (cheshire/decode true)
                 :count)))))
