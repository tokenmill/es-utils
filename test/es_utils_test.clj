(ns es-utils-test
  (:require [clojure.test :refer :all]
            [clojure.java.io :as io]
            [es-utils :as es]))

(deftest main-test
  (is (string? (slurp (es/as-input-stream "index-conf.json"))))
  (is (string? (slurp (es/as-input-stream (io/file "project.clj")))))

  (testing "if index name is correct"
    (is (= "docs_v4" (es/data->index-name "test-docs.json")))
    (is (= "docs_v4" (es/data->index-name (io/file "test/resources/test-docs.json"))))))

(deftest deletion-of-docs
  (let [conf {:host-1 {:host  "host-1"
                       :alias "docs-1"
                       :conf  "docs-1.json"
                       :data  "test-docs-1.json"}
              :host-2 {:host  "host-2"
                       :alias "docs-2"
                       :conf  "docs-2.json"
                       :data  "test-docs-2.json"}
              :no-host {:alias "docs-3"
                        :conf  "docs-3.json"
                        :data  "test-docs-3.json"}}
        host-indices (es/confs->host-index "default-host" conf)]
    (is (= 3 (count host-indices)))
    (is (= ["docs-1"] (-> host-indices (get "host-1"))))
    (is (= ["docs-2"] (-> host-indices (get "host-2"))))
    (is (= ["docs-3"] (-> host-indices (get "default-host"))))))
