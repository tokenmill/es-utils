run-dev-env:
	docker-compose -f docker-compose.yml -f docker-compose.dev.yml pull && \
	docker-compose -f docker-compose.yml -f docker-compose.dev.yml down && \
	docker-compose -f docker-compose.yml -f docker-compose.dev.yml build && \
	docker-compose -f docker-compose.yml -f docker-compose.dev.yml up --remove-orphans

build-test-docker:
	docker build -f Dockerfile.test -t registry.gitlab.com/tokenmill/es-utils/es-utils:test .

run-integration-tests: build-test-docker
	docker-compose -p testing -f docker-compose.yml -f docker-compose.test.yml down --remove-orphans
	docker-compose -p testing -f docker-compose.yml -f docker-compose.test.yml build
	docker-compose -p testing -f docker-compose.yml -f docker-compose.test.yml up --remove-orphans --abort-on-container-exit --exit-code-from es-utils

publish-in-clojars:
	lein deploy clojars
