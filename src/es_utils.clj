(ns es-utils
  (:require [clojure.tools.logging :as log]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [cheshire.core :as cheshire]
            [org.httpkit.client :as http])
  (:import (java.io File)))

(defn service-ready?
  ([endpoint]
   (service-ready? endpoint "service"))
  ([endpoint service-name]
   (service-ready? endpoint service-name 15))
  ([endpoint service-name timeout-seconds]
   (loop [{:keys [status]} @(http/request {:url endpoint})
          idx 0]
     (if (= 200 status)
       true
       (if (< timeout-seconds idx)
         false
         (do
           (log/warnf "Waiting for '%s' on '%s'." service-name endpoint)
           (Thread/sleep 1000)
           (recur @(http/request {:url endpoint}) (inc idx))))))))

(defn es-ready?
  ([es-host] (es-ready? es-host "Elasticsearch" 15))
  ([es-host es-name] (service-ready? es-host es-name 15))
  ([es-host es-name timeout] (service-ready? es-host es-name timeout)))

(defn put-alias [es-host index-name alias]
  @(http/request
     {:url     (format "%s/%s/_alias/%s" es-host index-name alias)
      :method  :put
      :headers {"Content-Type" "application/json"}}))

(defn as-input-stream
  "Given a path returns an input-stream"
  [index-config]
  (cond
    (string? index-config) (io/input-stream (io/resource index-config))
    (instance? File index-config) (io/input-stream index-config)))

(defn create-index [es-host index-name index-config]
  @(http/request
     {:url     (format "%s/%s" es-host index-name)
      :method  :put
      :headers {"Content-Type" "application/json"}
      :body    (as-input-stream index-config)}))

(defn data->index-name [data-file]
  (-> data-file (as-input-stream) (io/reader) (line-seq) (first) (cheshire/decode true) :index :_index))

(defn import-test-data-to-es [es-host es-index-name input-file]
  (let [resp (-> @(http/request
                    {:url     (format "%s/_bulk" es-host)
                     :method  :post
                     :headers {"Content-Type" "application/x-ndjson"}
                     :body    (as-input-stream input-file)})
                 :body
                 (cheshire/decode))]

    (if (get resp "errors")
      (do
        (log/warnf "Data upload from file '%s' failed for '%s/%s'"
                   input-file (count (filter #(get-in % ["index" "error"]) (get resp "items"))) (count (get resp "items")))
        (doseq [item (get resp "items")]
          (when (get-in item ["index" "error"])
            (log/warnf "Failed to upload: %s" (pr-str item)))))
      (log/infof "Data upload from file '%s' was succesfull for items count '%s'"
                 input-file (count (get resp "items")))))
  (let [resp @(http/request
                {:url    (format "%s/%s/_refresh" es-host es-index-name)
                 :method :post})]
    (log/infof "Refresh for index '%s' resulted in status '%s'" es-index-name (:status resp))))

(defn exists? [es-host index-name]
  (when (= 200 (:status @(http/request {:url (format "%s/%s" es-host index-name) :method :head})))
    true))

(defn actual-index-name [es-host index-name]
  (-> @(http/request {:url (format "%s/%s" es-host index-name)
                      :method :get})
      :body
      (cheshire/decode true)
      (vals)
      (first)
      :settings
      :index
      :provided_name))

(defn configure-index [es-host {:keys [alias conf data] :as index-conf}]
  (try
    (let [index-name (if data (data->index-name data) alias)]
      (when (exists? es-host index-name)
        (let [physical-index-name (actual-index-name es-host index-name)]
          (log/infof "Index '%s' deleted '%s'"
                     physical-index-name
                     (-> @(http/request {:url    (format "%s/%s" es-host physical-index-name)
                                         :method :delete})
                         :body
                         (cheshire/decode true)))))
      (let [{:keys [status body]} (create-index es-host index-name conf)]
        (if (= 200 status)
          (log/infof "Index '%s' with conf '%s' created with status '%s'"
                     index-name index-conf status)
          (log/errorf "Index '%s' creation with conf '%s' ended with status '%s' and body '%s'"
                      index-name index-conf status body)))
      (when-not (= index-name alias)
        (log/infof "Alias '%s' was put on index '%s' with status '%s'" alias index-name
                   (:status (put-alias es-host index-name alias)))))
    (catch Exception e
      (.printStackTrace e))))

(defn prepare-indices [env es-host es-index-confs]
  (doseq [[k {:keys [host alias] :as v}] es-index-confs]
    (configure-index (or host es-host) (assoc v :alias (or (get env k) alias)))))

(defn load-test-data [env default-es-host index-confs]
  (doseq [[k {:keys [host alias data]}] index-confs]
    (when data
      (import-test-data-to-es (or host default-es-host) (or (get env k) alias) data))))

(defn delete-docs!
  "Params:
  - es-host: e.g. http://127.0.0.1:9200
  - indices: a colection of index names e.g. [\"index-1\" \"index-2\"]"
  [es-host indices]
  (when (seq indices)
    (let [concatenated-indices (str/join "," indices)
          resp @(http/request
                  {:url          (format "%s/%s/_delete_by_query?refresh&wait_for_completion=true"
                                         es-host concatenated-indices)
                   :method       :post
                   :body         (cheshire/encode {:query {:match_all {}}})
                   :headers {"Content-Type" "application/json; charset=UTF-8"}})]
      (if (or (seq (:failures resp)) (not= 200 (:status resp)))
        (log/errorf "Failed to delete docs from host '%s' with response: %s" es-host resp)
        (log/infof "Deleted documents from host '%s' indices '%s'." es-host (pr-str indices))))))

(defn confs->host-index [default-es-host es-index-confs]
  (let [host-indices (->> es-index-confs
                          (map (fn [[_ v]] v))
                          (group-by :host)
                          (reduce-kv (fn [m k v] (assoc m k (map :alias v))) {}))
        no-host (get host-indices nil)]
    (if (empty? no-host)
      host-indices
      (-> host-indices
          (assoc default-es-host no-host)
          (dissoc nil)))))

(defn delete-all-docs [es-host es-index-confs]
  (doseq [[host indices] (confs->host-index es-host es-index-confs)]
    (delete-docs! host indices)))

(defn export-docs-for-bulk-via-http
  "Example: (export-docs-for-bulk \"http://127.0.0.1:9200\" \"index\" {:query {:match_all {}} :size 1000} \"output.json\")"
  [es-host index-name query out-file]
  (let [op (fn [hit] {"index" {"_index" (:_index hit) "_type" (:_type hit) "_id" (:_id hit)}})]
    (try
      (let [hits (-> @(http/request {:url     (format "%s/%s/_search" es-host index-name)
                                     :method  :post
                                     :headers {"Content-Type" "application/json; charset=UTF-8"}
                                     :body    (cheshire/encode query)})
                     :body (cheshire/decode true) :hits :hits)
            ops (reduce #(conj %1 (op %2) (:_source %2)) [] hits)]
        (doseq [line ops]
          (spit out-file (str (cheshire/encode line) "\n") :append true)))
      (catch Exception e
        (log/errorf "ES export failed '%s' with exception %s" query e)))))
