(defproject lt.tokenmill/es-utils "0.2.4"
  :description "Utilities for Elasticsearch"

  :dependencies [[http-kit "2.3.0"]
                 [cheshire "5.10.0"]]

  :test-selectors {:default (complement :integration)
                   :integration :integration}

  :profiles {:dev {:dependencies  []
                   :resource-paths ["test/resources/" "resources/"]}
             :provided {:dependencies [[org.clojure/clojure "1.10.1"]
                                       [org.clojure/tools.logging "1.1.0"]]}})
